require_relative 'lib/advanced_recognizer'
require_relative 'lib/input_reader'

def print_rectangles_coordinates(input)
  AdvancedRecognizer.new(input).recognize.each.with_index(1) do |rectangle, i|
    puts "Rectangle ##{i}:"
    rectangle.each do |k, v|
      puts "  #{k}: #{v}"
    end
  end
end

print_rectangles_coordinates(InputReader.read_canvas)