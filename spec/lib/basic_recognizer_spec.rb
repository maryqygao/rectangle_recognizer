require_relative '../../lib/basic_recognizer'

RSpec.describe BasicRecognizer do
  subject { BasicRecognizer.new(input).recognize }

  context 'input has one rectangle' do
    let(:input) do
      [
          [1, 1, 1, 1, 1, 1, 1],
          [1, 1, 1, 1, 1, 1, 1],
          [1, 1, 1, 0, 0, 0, 1],
          [1, 1, 1, 0, 0, 0, 1],
          [1, 1, 1, 1, 1, 1, 1]
      ]
    end

    it 'returns rectangle with labelled properties' do
      expect(subject).to include(:col, :row, :width, :height)
    end

    it 'returns correct co-ordinates' do
      expect(subject).to eq({col: 3, row: 2, width: 3, height: 2})
    end
  end

  context 'input has no rectangles' do
    let(:input)  do
      [
          [1, 1, 1, 1, 1, 1, 1],
          [1, 1, 1, 1, 1, 1, 1],
          [1, 1, 1, 1, 1, 1, 1],
          [1, 1, 1, 1, 1, 1, 1],
          [1, 1, 1, 1, 1, 1, 1]
      ]
    end

    it 'returns nil' do
      expect(subject).to be(nil)
    end
  end

  context 'empty input canvas' do
    let(:input) { [] }

    it 'returns 0 rectangles' do
      expect(subject).to be(nil)
    end
  end
end