require_relative '../../lib/advanced_recognizer'

RSpec.describe AdvancedRecognizer do
  subject { AdvancedRecognizer.new(input).recognize }

  context 'input has 3 rectangles' do
    let(:input) do
      [
          [1, 1, 1, 1, 1, 1, 1],
          [1, 1, 1, 1, 1, 1, 1],
          [1, 1, 1, 0, 0, 0, 1],
          [1, 0, 1, 0, 0, 0, 1],
          [1, 0, 1, 1, 1, 1, 1],
          [1, 0, 1, 0, 0, 1, 1],
          [1, 1, 1, 0, 0, 1, 1],
          [1, 1, 1, 1, 1, 1, 1]
      ]
    end

    it 'returns 3 rectangles' do
      expect(subject.size).to eq(3)
    end

    it 'returns rectangle with labelled properties' do
      expect(subject[0]).to include(:col, :row, :width, :height)
    end

    it 'returns correct column number' do
      expect(subject.map { |r| r[:col] }).to eq([3, 1, 3])
    end

    it 'returns correct row number' do
      expect(subject.map { |r| r[:row] }).to eq([2, 3, 5])
    end

    it 'returns correct width' do
      expect(subject.map { |r| r[:width] }).to eq([3, 1, 2])
    end

    it 'returns correct height' do
      expect(subject.map { |r| r[:height] }).to eq([2, 3, 2])
    end
  end
  
  context 'input has no rectangles' do
    let(:input)  do
      [
          [1, 1, 1, 1, 1, 1, 1],
          [1, 1, 1, 1, 1, 1, 1],
          [1, 1, 1, 1, 1, 1, 1],
          [1, 1, 1, 1, 1, 1, 1],
          [1, 1, 1, 1, 1, 1, 1],
          [1, 1, 1, 1, 1, 1, 1],
          [1, 1, 1, 1, 1, 1, 1],
          [1, 1, 1, 1, 1, 1, 1]
      ]
    end

    it 'returns 0 rectangles' do
      expect(subject.size).to eq(0)
    end
  end

  context 'empty input canvas' do
    let(:input) { [] }

    it 'returns 0 rectangles' do
      expect(subject.size).to eq(0)
    end
  end
end