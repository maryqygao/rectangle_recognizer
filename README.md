# Rectangle Recognizer

This repository contains two rectangle recognizer scripts 
that takes a 2D array of black and white cells as input, 
and outputs the co-ordinates of any rectangles it finds. 
The basic script recognizes a single black rectangle among white cells, 
while the advanced recognizes multiple black rectangles. 

## Run

To run the basic script, run in the shell:

```
$ ruby run_basic_recognize.rb < input_file
```

To run the advanced script, run in the shell:

```
$ ruby run_advanced_recognize.rb < input_file
```

## Input

The input is provided from the command line, 
as a 2D array of 1s and 0s in the following format:

* The first line contains the width and height of the input 2D array, 
separated by a space

* The next lines contain the rows of the 2D array. Each cell can either be a 
1 (white) or a 0 (black). The cells are separated by spaces.

* The input is expected to have 1 or more black rectangles

* No other shapes are allowed as an input. 
(e.g. black triangles are not allowed as input)

## Output

The output is printed as attributes of the rectangles, in the following format:

* `col` represents the column of the left-most edge of the rectangle

* `row` represents the row of the top-most edge of the rectangle

* `width` represents the width of the rectangle in columns

* `height` represents the height of the rectangle in rows

For the basic recognizer, only a single rectangle is printed. 
For the advanced case, all rectangles are each printed in the above format.

## Example

Here is an example output of the advanced recognition script
(using the sample input included in this repository):

```
$ ruby run_advanced_recognize.rb < example_input_advanced
Rectangle #1:
  col: 3
  row: 2
  width: 3
  height: 2
Rectangle #2:
  col: 1
  row: 3
  width: 1
  height: 3
Rectangle #3:
  col: 3
  row: 5
  width: 2
  height: 2

```

## Testing

Tests for the basic and advanced recognizers are included under `spec/lib`. 
To run the tests and check that they pass, run in the shell:

```
$ rspec
```

## Questions

For any enquiries, please [contact me via email](mailto:maryqygao@gmail.com).