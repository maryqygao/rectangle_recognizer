require_relative 'basic_recognizer'

class AdvancedRecognizer
  def initialize(canvas)
    @canvas = canvas
  end

  def recognize
    working_canvas = Marshal.load(Marshal.dump(canvas))
    rectangles = []
    rectangle = BasicRecognizer.new(working_canvas).recognize
    while !rectangle.nil?
      rectangles << rectangle
      erase_rectangle(working_canvas, rectangle)
      rectangle = BasicRecognizer.new(working_canvas).recognize
    end
    rectangles
  end

  private

  attr_accessor :canvas, :working_canvas

  def erase_rectangle(canvas, col:, row:, width:, height:)
    canvas[row, height] = canvas[row, height].each do |row|
      row[col, width] = [1] * width
    end
    canvas
  end
end
