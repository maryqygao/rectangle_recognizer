class InputReader
  def self.read_canvas
    width, height = gets.chomp.split.map(&:to_i)
    Array.new(height) { |_| gets.chomp.split[0, width].map(&:to_i) }
  end
end