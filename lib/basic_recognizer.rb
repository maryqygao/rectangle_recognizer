class BasicRecognizer
  def initialize(canvas)
    @canvas = canvas
  end

  def recognize
    top_left = find_top_left(canvas)
    return nil if top_left.nil?
    top_left.merge(find_dimensions(canvas, top_left))
  end

  private

  attr_accessor :canvas

  def find_top_left(canvas)
    canvas.each_with_index do |row_items, row_index|
      col_index = row_items.index(0)
      return { col: col_index, row: row_index } if !col_index.nil?
    end
    nil
  end

  def find_dimensions(canvas, col:, row:)
    width = canvas[row][col..-1].index(1) || canvas[row].size - col
    height = canvas[row..-1].index { |row_items| row_items[col] == 1 } || canvas.size - row
    {
        width: width,
        height: height
    }
  end
end
