require_relative 'lib/basic_recognizer'
require_relative 'lib/input_reader'

def print_rectangle_coordinates(input)
  BasicRecognizer.new(input).recognize.each do |k, v|
    puts "#{k}: #{v}"
  end
end

print_rectangle_coordinates(InputReader.read_canvas)